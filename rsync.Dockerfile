FROM alpine:3.20 AS main
COPY rsync-ssh-config.txt /root/.ssh/config
RUN \
    apk add --no-cache coreutils rsync openssh-client \
    && mkdir -p /root/.ssh \
    && chmod 700 /root/.ssh
ENTRYPOINT ["/usr/bin/ssh-agent"]
