FROM alpine:3.20 AS build
WORKDIR /root/example-git-repo
COPY ./pre-commit-config.yaml ./.pre-commit-config.yaml
RUN \
    apk --no-cache add pre-commit git \
    && git init . -b develop \
    && pre-commit install-hooks

FROM alpine:3.20 as main
RUN apk --no-cache add pre-commit git
COPY --from=build /root/.cache/pre-commit /root/.cache/pre-commit
