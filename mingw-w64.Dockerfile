FROM alpine:3.20 AS base
ARG ARCH=x86_64
ARG CRT=msvcrt
ENV ARCH=$ARCH CRT=$CRT TARGET=$ARCH-w64-mingw32
ENV PREFIX=/opt/$TARGET
ENV PATH="/opt/$TARGET/bin:$PATH"

FROM base AS build
ENV \
    BINUTILS_VERSION=2.43 \
    GCC_VERSION=13.2.0 \
    MINGW_W64_VERSION=12.0.0 \
    SDL2_VERSION=2.30.9 \
    LIBUSB_VERSION=1.0.26

WORKDIR /opt

RUN \
    apk --no-cache add curl \
    && curl -L -o binutils-$BINUTILS_VERSION.tar.gz https://ftp.gnu.org/gnu/binutils/binutils-$BINUTILS_VERSION.tar.gz \
    && curl -L -o gcc-$GCC_VERSION.tar.xz https://ftp.gnu.org/gnu/gcc/gcc-$GCC_VERSION/gcc-$GCC_VERSION.tar.xz \
    && curl -L -o mingw-w64-v$MINGW_W64_VERSION.tar.bz2 https://sourceforge.net/projects/mingw-w64/files/mingw-w64/mingw-w64-release/mingw-w64-v$MINGW_W64_VERSION.tar.bz2 \
    && curl -L -o SDL2-$SDL2_VERSION.tar.gz https://libsdl.org/release/SDL2-$SDL2_VERSION.tar.gz \
    && curl -L -o libusb-$LIBUSB_VERSION.tar.bz2 https://github.com/libusb/libusb/releases/download/v$LIBUSB_VERSION/libusb-$LIBUSB_VERSION.tar.bz2 \
    && tar xaf binutils-$BINUTILS_VERSION.tar.gz \
    && tar xaf gcc-$GCC_VERSION.tar.xz \
    && tar xaf mingw-w64-v$MINGW_W64_VERSION.tar.bz2 \
    && tar xaf SDL2-$SDL2_VERSION.tar.gz \
    && tar xaf libusb-$LIBUSB_VERSION.tar.bz2

RUN apk --no-cache add \
    build-base cmake \
    zlib-dev isl-dev mpc1-dev mpfr-dev gmp-dev openmp-dev

WORKDIR /opt/build-binutils
RUN \
    ../binutils-$BINUTILS_VERSION/configure \
        --target="$TARGET" \
        --prefix="$PREFIX" \
        --enable-lto \
        --enable-plugins \
        --enable-deterministic-archives \
        --disable-shared \
        --disable-gdb \
        --disable-gprofng \
        --disable-multilib \
        --disable-nls \
        --disable-werror \
    && make MAKEINFO=true -O \
    && make MAKEINFO=true install

WORKDIR /opt/build-gcc
RUN \
    ../gcc-$GCC_VERSION/configure \
        --target="$TARGET" \
        --prefix="$PREFIX" \
        --with-dwarf2 \
        --with-system-zlib \
        --enable-static \
        --enable-languages=c,c++ \
        --enable-cloog-backend=isl \
        --enable-lto \
        --enable-libgomp \
        --disable-shared \
        --disable-multilib \
        --disable-threads \
    && make -O all-gcc \
    && make install-strip-gcc

WORKDIR /opt/build-mingw-headers
RUN \
    ../mingw-w64-v$MINGW_W64_VERSION/mingw-w64-headers/configure \
        --prefix="$PREFIX/$TARGET" \
        --host="$TARGET" \
        --enable-sdk=all \
        --with-default-msvcrt=$CRT \
    && make install

WORKDIR /opt/build-gcc
RUN \
    make -O all-target-libgcc \
    && make install-strip-target-libgcc

WORKDIR /opt/build-mingw-crt
RUN \
    if [ "$ARCH" = "i686" ]; then \
        ../mingw-w64-v$MINGW_W64_VERSION/mingw-w64-crt/configure \
            --prefix="$PREFIX/$TARGET" \
            --host="$TARGET" \
            --enable-wildcard \
            --enable-lib32 \
            --disable-lib64 \
            --with-default-msvcrt=$CRT; \
    else \
        ../mingw-w64-v$MINGW_W64_VERSION/mingw-w64-crt/configure \
            --prefix="$PREFIX/$TARGET" \
            --host="$TARGET" \
            --enable-wildcard \
            --disable-lib32 \
            --enable-lib64 \
            --with-default-msvcrt=$CRT; \
    fi \
    \
    && make -O \
    && make install

WORKDIR /opt/build-mingw-winpthreads
RUN \
    ../mingw-w64-v$MINGW_W64_VERSION/mingw-w64-libraries/winpthreads/configure \
        --prefix="$PREFIX/$TARGET" \
        --host="$TARGET" \
        --enable-static \
        --enable-shared \
    && make -O \
    && make install

WORKDIR /opt/build-gcc
RUN \
    make -O \
    && make install-strip

WORKDIR /opt/build-libusb
RUN \
    ../libusb-$LIBUSB_VERSION/configure \
        --target="$TARGET" \
        --host="$TARGET" \
        --prefix="$PREFIX/$TARGET" \
        --enable-shared \
        --enable-static \
    && make -O \
    && make install

# Toolchain is ready, we can copy the toolchain.
COPY ./toolchains/$TARGET.cmake "$PREFIX/$TARGET.cmake"
ENV CMAKE_TOOLCHAIN_FILE="$PREFIX/$TARGET.cmake"

WORKDIR /opt/build-sdl2
RUN \
    cmake -S ../SDL2-$SDL2_VERSION -B . \
        -DCMAKE_INSTALL_PREFIX="$PREFIX/$TARGET" \
        -DSDL_STATIC=OFF \
        -DSDL_RPATH=OFF \
    && cmake --build . \
    && cmake --install .

WORKDIR $PREFIX
RUN \
    rm -r "$PREFIX/share"

FROM base AS main
ENV \
    PKG_CONFIG_LIBDIR="$PREFIX/lib/pkgconfig:$PREFIX/share/pkgconfig" \
    PKG_CONFIG_SYSROOT_DIR="$PREFIX" \
    PKG_CONFIG_EXECUTABLE=pkgconf \
    CMAKE_TOOLCHAIN_FILE="$PREFIX/$TARGET.cmake"

RUN apk --no-cache add \
    build-base isl26 openmp cmake=3.29.3-r0 \
    pkgconf python3=3.12.8-r1 py3-toml=0.10.2-r7 git=2.45.2-r0
COPY --from=build "$PREFIX" "$PREFIX"
