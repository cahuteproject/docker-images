Docker images for Cahute
========================

This repository contains instructions to build Docker containers able to
build Cahute_ for various environments, used in its CI.

All Docker images described here are placed in the following Docker registry,
hosted by Gitlab::

    registry.gitlab.com/cahuteproject/docker-images/

Build images
------------

Build images are available to build Cahute itself.
They include the components referenced in `Building Cahute from source`_.

The available build images are the following:

``registry.gitlab.com/cahuteproject/docker-images/i686-w64-mingw32-msvcrt``
    Build image targeted towards 32-bit (x86) Windows using `MinGW-w64`_
    and the MSVCRT runtime, with the following components:

    * CMake_ 3.31.1;
    * git_ 2.45.2;
    * `MinGW-w64`_ 12.0.0;
    * Python_ 3.12.7;
    * `toml module for Python`_ 0.10.2;
    * SDL_ 2.30.9;
    * libusb_ 1.0.26.

``registry.gitlab.com/cahuteproject/docker-images/i686-w64-mingw32-ucrt``
    Build image targeted towards 32-bit (x86) Windows using `MinGW-w64`_
    and the UCRT runtime, with the following components:

    * CMake_ 3.31.1;
    * git_ 2.45.2;
    * `MinGW-w64`_ 12.0.0;
    * Python_ 3.12.7;
    * `toml module for Python`_ 0.10.2;
    * SDL_ 2.30.9;
    * libusb_ 1.0.26.

``registry.gitlab.com/cahuteproject/docker-images/x86_64-w64-mingw32-msvcrt``
    Build image targeted towards 64-bit (x64) Windows using `MinGW-w64`_
    and the MSVCRT runtime, with the following components:

    * CMake_ 3.31.1;
    * git_ 2.45.2;
    * `MinGW-w64`_ 12.0.0;
    * Python_ 3.12.7;
    * `toml module for Python`_ 0.10.2;
    * SDL_ 2.30.9;
    * libusb_ 1.0.26.

``registry.gitlab.com/cahuteproject/docker-images/x86_64-w64-mingw32-ucrt``
    Build image targeted towards 64-bit (x64) Windows using `MinGW-w64`_
    and the UCRT runtime, with the following components:

    * CMake_ 3.31.1;
    * git_ 2.45.2;
    * `MinGW-w64`_ 12.0.0;
    * Python_ 3.12.7;
    * `toml module for Python`_ 0.10.2;
    * SDL_ 2.30.9;
    * libusb_ 1.0.26.

``registry.gitlab.com/cahuteproject/docker-images/x86_64-pc-linux-gnu``
    Build image targeted towards x86_64 Linux using glibc,
    with the following components:

    * CMake_ 3.31.1;
    * git_ 2.45.2;
    * Linux_ headers 4.19;
    * `GNU libc (glibc) <glibc_>`_ 2.28;
    * Python_ 3.12.7;
    * `toml module for Python`_ 0.10.2;
    * SDL_ 2.30.9;
    * libusb_ 1.0.26.

``registry.gitlab.com/cahuteproject/docker-images/arm-pc-linux-gnueabihf``
    Build image targeted towards ARM Linux using glibc with EABI and
    Hard Float (HF), with the following components:

    * CMake_ 3.31.1;
    * git_ 2.45.2;
    * Linux_ headers 4.19;
    * `GNU libc (glibc) <glibc_>`_ 2.28;
    * Python_ 3.12.7;
    * `toml module for Python`_ 0.10.2;
    * SDL_ 2.30.9;
    * libusb_ 1.0.26.

.. _Cahute: https://gitlab.com/cahuteproject/cahute
.. _Building Cahute from source:
    https://next.cahuteproject.org/guides/build.html

.. _CMake: https://cmake.org/
.. _git: https://git-scm.com/
.. _Python: https://www.python.org/
.. _toml module for Python: https://pypi.org/project/toml/
.. _SDL: https://www.libsdl.org/
.. _libusb: https://libusb.info/

.. _MinGW-w64:
    https://www.mingw-w64.org/
.. _glibc:
    https://www.gnu.org/software/libc/
.. _Linux: https://kernel.org/
