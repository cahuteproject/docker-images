FROM python:3-alpine AS main
RUN python -m pip install --no-cache sphinx~=7.2 sphinxcontrib-mermaid furo
