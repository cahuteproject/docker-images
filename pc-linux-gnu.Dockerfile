FROM alpine:3.20 AS base
ARG ARCH=x86_64
ARG ABI=gnu
ENV ARCH=$ARCH TARGET=$ARCH-pc-linux-$ABI
ENV PREFIX=/opt/$TARGET
ENV PATH="/opt/$TARGET/bin:$PATH"

FROM base AS build
ENV \
    BINUTILS_VERSION=2.43 \
    GCC_VERSION=13.2.0 \
    LINUX_VERSION=4.19 \
    GLIBC_VERSION=2.28 \
    SDL2_VERSION=2.30.9 \
    LIBUSB_VERSION=1.0.26

WORKDIR /opt

RUN \
    apk --no-cache add curl \
    && curl -L -o binutils-$BINUTILS_VERSION.tar.gz https://ftp.gnu.org/gnu/binutils/binutils-$BINUTILS_VERSION.tar.gz \
    && curl -L -o gcc-$GCC_VERSION.tar.xz https://ftp.gnu.org/gnu/gcc/gcc-$GCC_VERSION/gcc-$GCC_VERSION.tar.xz \
    && curl -L -o linux-${LINUX_VERSION}.tar.xz https://cdn.kernel.org/pub/linux/kernel/v${LINUX_VERSION%%.*}.x/linux-${LINUX_VERSION}.tar.xz \
    && curl -L -o glibc-$GLIBC_VERSION.tar.bz2 https://ftp.gnu.org/gnu/glibc/glibc-$GLIBC_VERSION.tar.bz2 \
    && curl -L -o SDL2-$SDL2_VERSION.tar.gz https://libsdl.org/release/SDL2-$SDL2_VERSION.tar.gz \
    && curl -L -o libusb-$LIBUSB_VERSION.tar.bz2 https://github.com/libusb/libusb/releases/download/v$LIBUSB_VERSION/libusb-$LIBUSB_VERSION.tar.bz2 \
    && tar xaf binutils-$BINUTILS_VERSION.tar.gz \
    && tar xaf gcc-$GCC_VERSION.tar.xz \
    && tar xaf linux-${LINUX_VERSION}.tar.xz \
    && tar xaf glibc-$GLIBC_VERSION.tar.bz2 \
    && tar xaf SDL2-$SDL2_VERSION.tar.gz \
    && tar xaf libusb-$LIBUSB_VERSION.tar.bz2

RUN apk --no-cache add \
    build-base linux-headers bison gawk cmake makedepend \
    gettext-dev zlib-dev isl-dev mpc1-dev mpfr-dev gmp-dev openmp-dev

WORKDIR /opt/build-binutils
RUN \
    ../binutils-$BINUTILS_VERSION/configure \
        --target="$TARGET" \
        --prefix="$PREFIX" \
        --enable-lto \
        --enable-plugins \
        --enable-deterministic-archives \
        --disable-shared \
        --disable-gdb \
        --disable-gprofng \
        --disable-multilib \
        --disable-nls \
        --disable-werror \
    && make MAKEINFO=true -O \
    && make MAKEINFO=true install

WORKDIR /opt/linux-$LINUX_VERSION
RUN \
    make ARCH="$ARCH" INSTALL_HDR_PATH="$PREFIX/$TARGET" headers_install

WORKDIR /opt/build-gcc
RUN \
    echo >"$PREFIX/$TARGET/include/limits.h" \
    && ../gcc-$GCC_VERSION/configure \
        --target="$TARGET" \
        --prefix="$PREFIX" \
        --with-headers="$PREFIX/$TARGET/include" \
        --with-dwarf2 \
        --enable-static \
        --enable-languages=c,c++ \
        --enable-cloog-backend=isl \
        --enable-lto \
        --enable-libgomp \
        --disable-shared \
        --disable-multilib \
        --disable-threads \
        --disable-libsanitizer \
    && make -O all-gcc \
    && make install-strip-gcc

WORKDIR /opt/build-glibc
RUN \
    ../glibc-$GLIBC_VERSION/configure \
        --target="$TARGET" \
        --host="$TARGET" \
        --build="$MACHTYPE" \
        --prefix="$PREFIX/$TARGET" \
        --without-selinux \
        --enable-add-ons \
        --enable-kernel=4.9.337 \
        --disable-werror \
        libc_cv_forced_unwind=yes \
    && echo "build-programs=no" >>configparms \
    && make install-headers cross-compiling=yes -O

WORKDIR /opt/build-gcc
RUN \
    echo >"$PREFIX/$TARGET/include/gnu/stubs.h" \
    && make -O all-target-libgcc \
    && make install-strip-target-libgcc

WORKDIR /opt/build-glibc
RUN \
    make -O \
    && make install

WORKDIR /opt/build-gcc
RUN \
    make -O \
    && make install-strip

WORKDIR $PREFIX
RUN rm -r "$PREFIX/$TARGET/sys-include"

WORKDIR /opt/build-libusb
RUN \
    ../libusb-$LIBUSB_VERSION/configure \
        --target="$TARGET" \
        --host="$TARGET" \
        --prefix="$PREFIX/$TARGET" \
        --enable-shared \
        --enable-static \
        --disable-udev \
    && make -O \
    && make install

COPY ./toolchains/$TARGET.cmake "$PREFIX/$TARGET.cmake"
ENV CMAKE_TOOLCHAIN_FILE="$PREFIX/$TARGET.cmake"

WORKDIR /opt/build-sdl2
RUN \
    cmake -S ../SDL2-$SDL2_VERSION -B . \
        -DCMAKE_INSTALL_PREFIX="$PREFIX/$TARGET" \
        -DSDL_STATIC=OFF \
        -DSDL_RPATH=OFF \
    && cmake --build . \
    && cmake --install .

WORKDIR $PREFIX
RUN \
    rm -r "$PREFIX/share" "$PREFIX/$TARGET/share" "$PREFIX/$TARGET/etc"

FROM base AS main
ENV \
    PKG_CONFIG_LIBDIR="$PREFIX/lib/pkgconfig:$PREFIX/share/pkgconfig" \
    PKG_CONFIG_SYSROOT_DIR="$PREFIX" \
    PKG_CONFIG_EXECUTABLE=pkgconf \
    CMAKE_TOOLCHAIN_FILE="$PREFIX/$TARGET.cmake"

RUN apk --no-cache add \
    build-base isl26 openmp cmake=3.29.3-r0 \
    gettext pkgconf python3=3.12.8-r1 py3-toml=0.10.2-r7 git=2.45.2-r0
COPY --from=build "$PREFIX" "$PREFIX"
